import { useDispatch } from 'react-redux'
import {
    addTodoListItemRemote,
    deleteTodoListItem,
    updateTodoListItem,
    getTodoListRemote,
    getTodoItemById,
} from '../api'
import { setTodoList } from '../store/todoListSlice'

const useTodos = () => {
    const dispatch = useDispatch()
    const getTodoListHook = async () => {
        const { data } = await getTodoListRemote()
        dispatch(setTodoList(data))
        return
    }
    const deleteTodoListItemHook = async (id) => {
        await deleteTodoListItem(id)
        getTodoListHook()
        return
    }
    const addTodoListItemHook = async (text) => {
        await addTodoListItemRemote({
            id: Math.ceil(Math.random() * 1000000),
            text: text,
            done: false,
        })
        getTodoListHook()
        return
    }
    const putTodoListItemHook = async (id, params) => {
        await updateTodoListItem(id, params)
        getTodoListHook()
        return
    }
    const getTodoListByIdHook = async (id) => {
        const respond = await getTodoItemById(id)
        getTodoListHook()
        return respond.date
    }
    return {
        getTodoListHook,
        deleteTodoListItemHook,
        addTodoListItemHook,
        putTodoListItemHook,
        getTodoListByIdHook,
    }
}
export default useTodos
