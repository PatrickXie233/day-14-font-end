import api from './api'
const getTodoListRemote = () => {
    return api.get('/todos')
}
const addTodoListItemRemote = (todoItem) => {
    return api.post('/todos', todoItem)
}
const deleteTodoListItem = (id) => {
    return api.delete(`/todos/${id}`)
}
const updateTodoListItem = (id, params) => {
    return api.put(`/todos/${id}`, params)
}
const getTodoItemById = (id) => {
    return api.get(`/todos/${id}`)
}

export {
    getTodoListRemote,
    addTodoListItemRemote,
    deleteTodoListItem,
    updateTodoListItem,
    getTodoItemById,
}
