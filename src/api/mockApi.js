import axios from 'axios'

const mockApi = axios.create({
    baseURL: 'https://64c0b6670d8e251fd11263d8.mockapi.io/api',
})
export default mockApi
