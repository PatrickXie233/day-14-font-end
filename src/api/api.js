import axios from 'axios'

const mockApi = axios.create({
    baseURL: 'http://localhost:8080',
})
export default mockApi
