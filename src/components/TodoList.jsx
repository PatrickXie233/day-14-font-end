import { useEffect } from 'react'
import TodoGenerator from './TodoGenerator'
import TodoGroup from './TodoGroup/index'
import useTodos from '../Hooks/useTodos'
const TodoList = () => {
    const { getTodoListHook } = useTodos()
    useEffect(() => {
        getTodoListHook()
    }, [])

    return (
        <div className="">
            <h1> Todo List </h1>
            <TodoGenerator></TodoGenerator>
            <TodoGroup></TodoGroup>
        </div>
    )
}
export default TodoList
